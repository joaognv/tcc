-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 25-Maio-2014 às 14:14
-- Versão do servidor: 5.5.33
-- versão do PHP: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `phpag839_tcc`
--

CREATE DATABASE IF NOT EXISTS tcc;

USE tcc;


-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `info`) VALUES
(1, 'Smartphone CCE Motion Plus SK351 Desbloqueado com Dual Chip, Android 4.0, Tela 3,5", 3G/Wi-Fi, Câmera 2MP e GPS - Preto\r\nDe: R$ 399,00\r\nPor:R$ 249,00'),
(2, 'Panela Elétrica de Arroz com Capacidade de 10 Xícaras e Cozi Vapor NKS Mais Você TSK-2866\r\nPanela Elétrica de Arroz com Capacidade de 10 Xícaras e Cozi Vapor NKS Mais Você TSK-2866\r\nDe: R$ 139,90\r\nPor:R$ 99,90');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
