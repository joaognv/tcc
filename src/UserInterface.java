import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class UserInterface extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Início da Aplicação
	 */
	public static void main(String[] args) {
				
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				try {
					// Instancia uma classe da interface com o usuário	
					UserInterface frame = new UserInterface();
					
					// Instancia uma classe do menu principal da aplicação
					MenuPrincipal menuPrincipal = new MenuPrincipal();
					
					// Adiciona o menu principal à interface com o usuário
					frame.getContentPane().add(menuPrincipal);
					
					// Torna a interface com o usuário visível
					frame.setVisible(true);		
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		});
	}

	/**
	 * Cria a interface com o usuário
	 */
	public UserInterface() { 
		
		// Não será redimensionável
		setResizable(false);
		
		// Define o título da aplicação
		setTitle("Atualizar as informações de um produto no estoque");
		
		// Diz que quando o frame for fechado, o programa será encerrado
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Define que o frame irá abrir nas coordenadas 100x100 e terá tamanho 400x300 pixels
		setBounds(100, 100, 400, 300);
		
		// Instancia um painel para o frame, que receberá os objetos de interface
		contentPane = new JPanel();
		
		// Define as bordas do painel e o tipo de layout para ele
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		
		// Inclui o painel recém criado na interface com o usuário
		setContentPane(contentPane);
	}

}
