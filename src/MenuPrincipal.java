import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextPane;

import java.awt.Color;

import javax.swing.JTextField;


public class MenuPrincipal extends JPanel {

	private static final long serialVersionUID = 1L;
	
	// Declara o campo de texto que receberá o código do produto digitado pelo usuário
	private JTextField txt_prodCod;

	/**
	 * Cria o painel
	 */
	public MenuPrincipal() {
		
		// Define o layout como absoluto, assim pode-se posicionar os elementos por coordenadas
		setLayout(null);
		
		// Declara a label do campo do código do produto
		final JLabel lblInsiraCodigoProduto = new JLabel("Insira o código do produto");
		
		// Define o tamanho, posição e fonte da label do campo do código do produto
		lblInsiraCodigoProduto.setBounds(26, 12, 304, 26);
		lblInsiraCodigoProduto.setFont(new Font("Verdana", Font.BOLD, 20));

		// Adiciona a label ao painel
		add(lblInsiraCodigoProduto);
		
		// Declara o botão "Consultar Produto"
		JButton btnConsultarProduto = new JButton("Consultar Produto");
		
		// Adiciona um manipulador de eventos para o botão
		btnConsultarProduto.addActionListener(new ActionListener() {

			// Especifica a ação quando o usuário interagir com o botão
			public void actionPerformed(ActionEvent e) {
				try{
					
					// Converte o código digitado pelo usuário de String para inteiro  
					int produtoId = Integer.parseInt(txt_prodCod.getText());
					
					// Assim ele poderá ser usado no método que trará o produto do banco de dados
					new EditProduto(produtoId);
					
				}catch(NumberFormatException e1){
					
					// Caso o código digitado pelo usuário não seja numérico, exibe um alerta para o usuário
					JOptionPane.showMessageDialog(null, "O código de produtos é composto apenas por números.");
					
				}
				
			}
		});
		
		// Define o tamanho, a posição e adiciona o botão ao painel
		btnConsultarProduto.setBounds(96, 95, 164, 25);
		add(btnConsultarProduto);
		
		// Declara o botão "Sair"
		JButton btnSair = new JButton("Sair");
		
		// Adiciona um manipulador de eventos para o botão
		btnSair.addActionListener(new ActionListener() {
			
			// Especifica a ação quando o usuário interagir com o botão
			public void actionPerformed(ActionEvent e) {
				
				// Encerra a aplicação
				System.exit(0);
			}
		});
		
		// Define o tamanho, a posição e adiciona o botão ao painel
		btnSair.setBounds(147, 132, 62, 25);
		add(btnSair);
		
		// Declara o painel de texto contendo os Créditos da Aplicação 
		JTextPane txtpnCreditos = new JTextPane();
		
		// Define a fonte do texto, a cor do fundo, o texto que será exibido e a posição e tamanho do elemento
		txtpnCreditos.setFont(new Font("Dialog", Font.PLAIN, 10));
		txtpnCreditos.setBackground(Color.LIGHT_GRAY);
		txtpnCreditos.setText("Desenvolvido no primeiro semestre de 2014 por: Leonardo Colpas, Leandro Martins, Fernanda Aquino, João Gabriel Neves Vieira.\n\n                      Centro Universitário Carioca");
		txtpnCreditos.setBounds(35, 178, 287, 81);
		
		// Adiciona o painel de texto ao painel do menu
		add(txtpnCreditos);
		
		// Declara o campo de texto que receberá o código do produto
		txt_prodCod = new JTextField();
		
		// Define a posição, altura e largura do campo de texto
		txt_prodCod.setBounds(96, 69, 164, 25);

		// Adiciona o campo de texto ao painel
		add(txt_prodCod);
		
		// Define o número de colunas do campo de texto
		txt_prodCod.setColumns(10);

	}
}
