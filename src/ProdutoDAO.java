import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProdutoDAO {

	private Statement comando = null;  
	private Connection conn = null;	

	public Produto consultar(int id) throws InstantiationException,	SQLException {  
		
		// Executa o método conectar() para se conectar ao banco de dados
		conectar();  
		
		// Declara a variável que irá receber o resultado da consulta ao banco
		ResultSet rs;  

		// Executa a query de seleção de produto baseado no id do produto desejado
		rs = comando.executeQuery("SELECT * FROM produto WHERE id = " + id);  
		
		// Aponta para o primeiro (e única) linha retornada pela consulta
		rs.first();

		// Declara um objeto temporário da classe Produto
		Produto temp = new Produto();    
		
		// Define o ID deste produto com o valor retornado do banco
		temp.setID(rs.getInt("id"));  
		
		// Define as informações deste produto com o valor retornado do banco
		temp.setInfo(rs.getString("info"));
		
		// Define o preço deste produto com o valor retornado do banco
		temp.setPreco(rs.getFloat("preco"));
		
		// Define a data de validade deste produto com o valor retornado do banco
		temp.setValidade(rs.getString("validade"));
		
		// Define a data de fabricação deste produto com o valor retornado do banco
		temp.setFabricacao(rs.getString("fabricacao"));
		
		
		// Executa o método fechar() para fechar a conexão com o banco
		fechar();
		
		// Retorna o objeto Produto
		return temp;
		
	} 

	public void atualizar(Produto produto) {  
		
		try {
			
			// Executa a conexão com o banco de dados
			conectar();
			
		} catch (InstantiationException e1) {
			
			// Caso aconteça um erro durante a conexão, exibe informações úteis à depuração
			e1.printStackTrace();
		}  
		
		// Define a query que será executada a partir do produto passado como 
		// parâmetro atualizando as informações do produto 
		String com = "UPDATE produto SET info = '" + produto.getInfo() + "', fabricacao = '" + produto.getFabricacao() + "', validade = '" + produto.getValidade() + "', preco = "+produto.getPreco()+" WHERE  id = '" + produto.getID() + "';";  
	
		try {
			
			// Executa a Atualização
			comando.executeUpdate(com);  
			
		} catch (SQLException e) {
			
			// Caso aconteça um erro durante a execução da query, exibe informações úteis à depuração
			e.printStackTrace();  
			
		} finally {  
			
			// Depois de executada a query, fecha a conexão com o banco
			fechar(); 
			
		}  
	}  


	private void conectar() throws InstantiationException {  

		try {
			
			// Instancia e carrega na memória 
			new com.mysql.jdbc.Driver();
			
		} catch (SQLException e) {
			
			// Caso aconteça um erro durante a execução, exibe informações úteis à depuração
			e.printStackTrace();
		}
		// Configura o local, porta e o nome do banco de dados (localhost, 3306 e tcc, respectivamente)
		String connectionUrl = "jdbc:mysql://localhost:3306/tcc";
		
		// Usuário e senha do banco
		String connectionUser = "root";
		String connectionPassword = "";

		try {
			this.conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			this.comando = this.conn.createStatement();  
		} catch (SQLException e) {
			
			// Caso aconteça um erro durante a execução, exibe informações úteis à depuração
			e.printStackTrace();
		}


	}  

	private void fechar() {  
		try {  
			
			// Fecha a variável de comando e a conexão com o banco
			this.comando.close();
			this.conn.close();  
		} catch (SQLException e) {  
			
			// Caso aconteça um erro durante a execução, exibe informações úteis à depuração
			System.out.println(e.getMessage());  
			
		}  
	}  



}