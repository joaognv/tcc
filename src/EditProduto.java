import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;


public class EditProduto extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtID;
	private JLabel lblInformaesDoProduto;
	private JButton btnSair;
	private JButton btnAlterarProduto;
	private JTextArea info;
	private JScrollPane scrollPane;
	private ProdutoDAO produtoDAO = new ProdutoDAO();
	private Produto produto = new Produto();
	private JTextField txt_preco;
	private JTextField txt_fab;
	private JTextField txt_val;

	public EditProduto(int produtoId) {

		// Define a ação quando a janela for fechada (dispensar o frame)
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// Define o tamenho e posição da janela
		setBounds(150, 150, 450, 376);
		
		// Declara o painel, adiciona ao frame e define o layout como absoluto
		// (para que se possa posicionar os elementos definindo suas coordenadas)
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Declara o botão "Sair", adiciona um manipulador de eventos, define a ação que será disparada pelo evento
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		btnSair.setActionCommand("SAIR");
		
		// Define a posição, tamanho e adiciona o botão ao painel
		btnSair.setBounds(12, 313, 117, 25);
		contentPane.add(btnSair);			

		// Declara a label do campo do código do produto, define o tamanho, posição e adiciona a label ao painel
		JLabel lblCdigoDoProduto = new JLabel("Código do Produto");
		lblCdigoDoProduto.setBounds(12, 12, 132, 15);
		contentPane.add(lblCdigoDoProduto);

		// Declara o campo de texto que receberá o código do produto
		txtID = new JTextField();
		
		// Define que não será editável, sua posição, tamanho e adiciona ao painel
		txtID.setEditable(false);
		txtID.setBounds(162, 10, 114, 19);
		contentPane.add(txtID);
		
		// Define o número de colunas do campo de texto
		txtID.setColumns(10);

		// Declara a label das Informações do Produto, sua posição, tamanho e adiciona ao painel
		lblInformaesDoProduto = new JLabel("Informações do Produto");
		lblInformaesDoProduto.setBounds(12, 100, 170, 15);
		contentPane.add(lblInformaesDoProduto);

		// Delara o botão "Alterar Produto", adiciona um manipulador de eventos e a ação que será disparada pelo evento
		btnAlterarProduto = new JButton("Alterar Produto");
		btnAlterarProduto.addActionListener(this);
		btnAlterarProduto.setActionCommand("ALTERAR");
		
		// Define o tamanho, posição e adiciona o botão ao painel
		btnAlterarProduto.setBounds(290, 313, 144, 25);
		contentPane.add(btnAlterarProduto);

		// Declara uma área de Scroll, define sua posição e tamanho e adiciona ao painel
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 120, 422, 159);
		contentPane.add(scrollPane);

		// Declara a área de Texto que receberá as informações do produto e insere dentro da área de scroll
		info = new JTextArea();
		scrollPane.setViewportView(info);
		
		JLabel lblPreco = new JLabel("Preço");
		lblPreco.setBounds(294, 12, 57, 15);
		contentPane.add(lblPreco);
		
		txt_preco = new JTextField();
		txt_preco.setBounds(348, 10, 68, 19);
		contentPane.add(txt_preco);
		txt_preco.setColumns(10);

		JLabel lblDataDeFabricao = new JLabel("Data de Fabricação");
		lblDataDeFabricao.setBounds(12, 43, 153, 15);
		contentPane.add(lblDataDeFabricao);
		
		txt_fab = new JTextField();
		txt_fab.setBounds(162, 41, 114, 19);
		contentPane.add(txt_fab);
		txt_fab.setColumns(10);
		
		JLabel lblDataDeValidade = new JLabel("Data de Validade");
		lblDataDeValidade.setBounds(12, 73, 137, 15);
		contentPane.add(lblDataDeValidade);
		
		txt_val = new JTextField();
		txt_val.setBounds(162, 69, 114, 19);
		contentPane.add(txt_val);
		txt_val.setColumns(10);
				
		
		
		try {
			
			// Traz o produto a partir do ID recebido 
			produto = produtoDAO.consultar(produtoId);

			// Define o texto do campo "Código do Produto" a partir do produto retornado do banco
			txtID.setText(Integer.toString(produto.getID()));
			
			// Define o texto do campo "Informações do Produto" a partir do produto retornado do banco
			info.setText(produto.getInfo());

			// Define o texto do campo "Preço" a partir do produto retornado do banco
			String preco = Float.toString(produto.getPreco());
			txt_preco.setText(preco);			
			
			// Define o texto do campo "Data de Validade" a partir do produto retornado do banco
			txt_val.setText(produto.getValidade());	

			// Define o texto do campo "Data de Fabricação" a partir do produto retornado do banco
			txt_fab.setText(produto.getFabricacao());	
			
			
			// Exibe o painel com os dados atualizados
			this.setVisible(true);

		} catch (InstantiationException | SQLException e) {

			// Caso o produto não seja encontrado, ou qualquer outro erro aconteça, exibe uma mensagem ao usuário
			JOptionPane.showMessageDialog(null, "Produto não encontrado\nPor favor, verifique o código.");

		}

	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		// Verifica se a ação disparada pelo evento é SAIR
		if (e.getActionCommand()=="SAIR"){
			
			// Se for, Fecha esta janela
			this.dispose();
			
			// Verifica se a ação disparada pelo evento é ALTERAR
		}else if(e.getActionCommand()=="ALTERAR"){
			
			// Se for, define as Informações do objeto produto com o que está escrito no campo "Informações do Produto"
			produto.setInfo(info.getText());
			float preco = Float.parseFloat(txt_preco.getText());
			produto.setPreco(preco);
			
			produto.setValidade(txt_val.getText());
			produto.setFabricacao(txt_fab.getText());
			
			// Executa o método atualizar que altera o registro do produto no banco de dados
			produtoDAO.atualizar(produto);
			
			// Exibe uma mensagem de sucesso ao usuário e fecha esta janela
			JOptionPane.showMessageDialog(null, "Produto alterado com sucesso");
			this.dispose();
			
		}

	}
}
