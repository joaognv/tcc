
public class Produto {
	
	// Atributos do objeto Produto
	private int ID;
	private String info = null;
	private float preco;
	private String validade = null;
	private String fabricacao = null;
	
	// Retorna o ID do produto
	public int getID() {
		return ID;
	}

	// Define o ID do produto
	public void setID(int iD) {
		ID = iD;
	}


	// Retorna as informações do produto
	public String getInfo() {
		return info;
	}


	// Define as informações do produto
	public void setInfo(String info) {
		this.info = info;
	}

	// Retorna o preço do produto
	public float getPreco() {
		return preco;
	}

	// Define o preço do produto
	public void setPreco(float preco) {
		this.preco = preco;
	}

	public String getValidade() {
		return validade;
	}

	public void setValidade(String validade) {
		this.validade = validade;
	}

	public String getFabricacao() {
		return fabricacao;
	}

	public void setFabricacao(String fabricacao) {
		this.fabricacao = fabricacao;
	}


}
